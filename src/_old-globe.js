import * as THREE from 'three';

const MAP = 'https://upload.wikimedia.org/wikipedia/commons/f/f4/Mercator_projection_SW.jpg';
const R = 1;

(() => {
  const canvas = document.querySelector('#map');
  const renderer = new THREE.WebGLRenderer({ canvas });

  const camera = new THREE.PerspectiveCamera(75, 2, 0.1, 5);
  camera.position.z = R;
  const scene = new THREE.Scene();
  const geometry = new THREE.SphereGeometry(R / 2.5, 128, 128);

  const loader = new THREE.TextureLoader();
  const material = new THREE.MeshBasicMaterial({
    map: loader.load(MAP),
  });
  const sphere = new THREE.Mesh(geometry, material);
  sphere.rotation.x = 0.5;
  scene.add(sphere);

  const resizeRendererToDisplaySize = ({ domElement: canvas }) => {
    const { width, height, clientWidth, clientHeight } = canvas;
    const needResize = width !== clientWidth || height !== clientHeight;
    if (needResize) {
      renderer.setSize(clientWidth, clientHeight, false);
    }
    return needResize;
  };

  const render = (time) => {
    if (resizeRendererToDisplaySize(renderer)) {
      const canvas = renderer.domElement;
      const { clientWidth, clientHeight } = canvas;
      camera.aspect = clientWidth / clientHeight;
      camera.updateProjectionMatrix();
    }
    // const rot = time / 3.5e4;
    sphere.rotation.y = -1.8;
    renderer.render(scene, camera);
    requestAnimationFrame(render);
  };
  requestAnimationFrame(render);

  const createPin = ({ x, y, z }) => {
    const geometry = new THREE.SphereGeometry(0.02, 32, 32);
    const material = new THREE.MeshBasicMaterial({ color: 0xac1344 });
    const pin = new THREE.Mesh(geometry, material);
    console.log('@createPin', { x, y, z });
    pin.position.set(x, y, z || 0);
    scene.add(pin);
  };

  const mouse = new THREE.Vector2();
  const onClick = (event) => {
    event.preventDefault();
    const { clientX, clientY } = event;
    const { domElement: { clientWidth, clientHeight } } = renderer;
    mouse.x = (clientX / clientWidth) * 2 - 1;
    mouse.y = -(clientY / clientHeight) * 2 + 1;
    const raycaster = new THREE.Raycaster();
    raycaster.setFromCamera(mouse, camera);
    const [intersect] = raycaster.intersectObjects(scene.children);
    console.log('@onClick', intersect);
    if (intersect) createPin(intersect.point);
  };
  const onMove = (event) => {
    const { clientX, clientY } = event;
    const { domElement: { clientWidth, clientHeight } } = renderer;
    mouse.x = (clientX / clientWidth) * 2 - 1;
    mouse.y = -(clientY / clientHeight) * 2 + 1;
    const raycaster = new THREE.Raycaster();
    raycaster.setFromCamera(mouse, camera);
    const [intersect] = raycaster.intersectObjects(scene.children);
    canvas.style.cursor = intersect ? 'pointer' : 'default';

    // TODO: move globe on drag
    const movementX = event.movementX || event.mozMovementX || event.webkitMovementX || 0;
    const movementY = event.movementY || event.mozMovementY || event.webkitMovementY || 0;
    const speed = 0.02;
    const sgnX = -1 * Math.sign(movementX);
    const sgnY = Math.sign(movementY);
    const { x, y, z } = camera.position;
    camera.position.set(
      x * Math.cos(sgnX * speed) + z * Math.sin(sgnX * speed),
      y * Math.cos(sgnY * speed) + z * Math.sin(sgnY * speed),
      z * Math.cos(sgnX * speed) - x * Math.sin(sgnX * speed) - y * Math.sin(sgnY * speed),
    );
    camera.lookAt(scene.position);
  };
  document.addEventListener('mousedown', onClick, { capture: false });
  document.addEventListener('mousemove', onMove, { capture: false });
})();
