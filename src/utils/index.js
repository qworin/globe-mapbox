import { MAPBOX_STYLE } from '../resources/constants';

const { cos, PI, sign, sin } = Math;
const { isNaN } = Number;
const LON = 180;
const LAT = 90;

export const validateStyle = (param) => {
  const def = MAPBOX_STYLE.default;
  const valids = Array.from(new Set(Object.values(MAPBOX_STYLE)));
  if (param && !valids.includes(param)) {
    console.warn(`Style expected to be one of: ${valids}; received ${param}. Set style as ${def}`);
    return def;
  }
  return param || def;
};

const validateUv = ({ x: rawX, y: rawY } = {}) => {
  const x = parseFloat(rawX);
  const y = parseFloat(rawY);
  if (x < 0 || x > 1 || y < 0 || y > 1 || isNaN(x) || isNaN(y)) {
    throw new RangeError(`UV coordinates expected to be in 0..1; received { x: ${rawX}, y: ${rawY}}`);
  }
  return { x, y };
};

export const validateCoords = (lonLatLike) => {
  let rawLon;
  let rawLat;
  if (Array.isArray(lonLatLike)) {
    [rawLon, rawLat] = lonLatLike;
  } else if (lonLatLike === Object(lonLatLike)) {
    rawLon = lonLatLike.lon || lonLatLike.lng;
    rawLat = lonLatLike.lat;
  } else {
    const ref = 'https://docs.mapbox.com/mapbox-gl-js/api/#lnglatlike';
    throw new TypeError(`Wrong parameter passed as geocoordinates: ${lonLatLike}; expected LngLatLike: ${ref}`);
  }
  const lon = parseFloat(rawLon);
  const lat = parseFloat(rawLat);
  if (lon < -LON || lon > LON || lat < -LAT || lat > LAT || isNaN(lon) || isNaN(lat)) {
    throw new RangeError(`Wrong geocoordinates passed: { lon: ${rawLon}, lat: ${rawLat} }`);
  }
  return { lon, lat };
};

export const coordsToUv = (coords) => {
  const { lon, lat } = validateCoords(coords);
  return {
    x: (lon + LON) / LON / 2,
    y: (lat + LAT) / LAT / 2,
  };
};

export const uvToCoords = (uv) => {
  const { x, y } = validateUv(uv);
  return {
    lon: (2 * x - 1) * LON,
    lat: (2 * y - 1) * LAT,
  };
};

export const uvToSpherical = (uv) => {
  const { x, y } = validateUv(uv);
  const phi = x * 2 * PI;
  const theta = (1 - y) * PI;
  return { theta, phi };
};

export const sphericalToCartesian = ({ theta, phi, radius = 1 } = {}) => {
  if (theta < 0 || theta > PI || phi < 0 || phi > 2 * PI || isNaN(theta) || isNaN(phi)) {
    throw new RangeError(`Wrong spherical coordinates passed: { theta: ${theta}, phi: ${phi} }`);
  }
  return {
    x: radius * sin(theta) * cos(phi),
    y: radius * sin(theta) * sin(phi),
    z: radius * cos(theta),
  };
};

export const uvToCartesian = (uv, radius = 1) => {
  const spherical = uvToSpherical(uv);
  return sphericalToCartesian({ ...spherical, radius });
};

/**
 * Old method of dragging the globe
 * It replaced with moveGlobe @renderer.js
 */
export const rotateCamera = ({ event, camera, scene }) => {
  const SPEED = 0.04;
  const movementX = event.movementX || event.mozMovementX || event.webkitMovementX || 0;
  const movementY = event.movementY || event.mozMovementY || event.webkitMovementY || 0;
  const sgnX = -1 * sign(movementX);
  const sgnY = sign(movementY);
  const { x, y, z } = camera.position;
  camera.position.set(
    x * cos(sgnX * SPEED) + z * sin(sgnX * SPEED),
    y * cos(sgnY * SPEED) + z * sin(sgnY * SPEED),
    z * cos(sgnX * SPEED) - x * sin(sgnX * SPEED) - y * sin(sgnY * SPEED),
  );
  camera.lookAt(scene.position);
};
