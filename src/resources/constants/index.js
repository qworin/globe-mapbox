export const MAPBOX_STYLE = {
  dark: 'dark',
  light: 'light',
  outdoors: 'outdoors',
  satellite: 'satellite',
  streets: 'streets',
  default: 'streets',
};

export const POLE_COLOR = {
  dark: { north: 0x181818, south: 0x080808 },
  light: { north: 0xd1d1d1, south: 0xeeeeee },
  outdoors: { north: 0xcbdddd, south: 0xf4f9ff },
  satellite: { north: 0x00030d, south: 0xf5f4f9 },
  streets: { north: 0x73b5e5, south: 0xedffff },
  default: { north: 0x73b5e5, south: 0xedffff },
};

export const CUSTOM_EVENTS = {
  changeView: 'changeview',
};
