import { renderGlobe } from './components/globe';
import { registerEvents } from './components/events';

const globeWrapper = document.querySelector('#globe');
const threeObjects = renderGlobe();
registerEvents(globeWrapper, threeObjects);
