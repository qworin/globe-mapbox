import * as THREE from 'three';

import { getGlobes } from './geosphere';
import { getRender } from './renderer';
import { MAPBOX_STYLE } from '../resources/constants';

const INIT_ZOOM = 1; // 0 looks bad
const CONTAINER = document.querySelector('#globe');

const globeAxis = new THREE.Vector3(0, 1, 0);
const tiltAxis = new THREE.Vector3(1, 0, 0);
export const renderGlobe = () => {
  const renderer = new THREE.WebGLRenderer({ canvas: CONTAINER });
  const camera = new THREE.PerspectiveCamera(75, 2, 0.1, 5);
  camera.position.z = 1;
  const scene = new THREE.Scene();
  const globes = getGlobes(MAPBOX_STYLE.default);
  scene.userData = { globes, isTouched: false, isPaused: false, mapZoom: INIT_ZOOM };
  scene.add(globes[INIT_ZOOM]);
  scene.rotateOnAxis(globeAxis, -2.2); // ~moscow longtitude
  scene.rotateOnWorldAxis(tiltAxis, 0.5); // just spectacular view
  const render = getRender({ renderer, scene, camera });
  render();
  console.log('_S_C_E_N_E_', scene);
  return { scene, camera, renderer };
};

export const REPLACE_ME = 'prefer default export';
