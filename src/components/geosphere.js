import * as THREE from 'three';

import { POLE_COLOR } from '../resources/constants';

const NAME_PREFIX = 'globe@';

const SRC = {
  dark: {
    zoom0: require('../resources/maps/dark@0.png'),
    zoom1: require('../resources/maps/dark@1.png'),
    zoom2: require('../resources/maps/dark@2.png'),
    zoom3: require('../resources/maps/dark@3.png'),
  },
  satellite: {
    zoom0: require('../resources/maps/satellite@0.jpg'), // sic!
    zoom1: require('../resources/maps/satellite@1.png'),
    zoom2: require('../resources/maps/satellite@2.png'),
    zoom3: require('../resources/maps/satellite@3.png'),
  },
  streets: {
    zoom0: require('../resources/maps/streets@0.png'),
    zoom1: require('../resources/maps/streets@1.png'),
    zoom2: require('../resources/maps/streets@2.png'),
    zoom3: require('../resources/maps/streets@3.png'),
  },
};

const RADIUS = 0.4; // for camera.position.z = 1
const getSphericalCap = (cap = 'default') => {
  /*
   * There is a cutoff for map from tiles (aka 'default')
   * At 85.0511deg N/S latitude = CUTOFF * 180 / PI
   * ref: https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#X_and_Y
   * */
  const { atan, sinh, PI } = Math;
  const CUTOFF = atan(sinh(PI));
  const SEGMENTS = 128; // roundness degree of rendered sphere
  let radius = RADIUS;
  if (cap !== 'default') {
    radius -= 0.001;
  }
  const thetaArgs = {
    default: [PI / 2 - CUTOFF, CUTOFF * 2],
    north: [0, PI / 2], // north semisphere
    south: [PI / 2, PI / 2], // south semisphere
  }[cap];
  const args = [radius, SEGMENTS, SEGMENTS, 0, PI * 2, ...thetaArgs];
  return new THREE.SphereGeometry(...args);
};

export const getGeosphere = ({ zoom = 0, style } = {}) => {
  const src = SRC[style][`zoom${zoom}`];
  const map = new THREE.TextureLoader().load(src);
  const materials = [
    new THREE.MeshBasicMaterial({ map }),
    new THREE.MeshBasicMaterial({ color: POLE_COLOR[style].north }),
    new THREE.MeshBasicMaterial({ color: POLE_COLOR[style].south }),
  ];
  const globe = getSphericalCap();
  const northPole = getSphericalCap('north');
  const southPole = getSphericalCap('south');
  const geometry = new THREE.Geometry();
  geometry.merge(globe, globe.matrix);
  geometry.merge(northPole, northPole.matrix, 1);
  geometry.merge(southPole, southPole.matrix, 2);
  const geosphere = new THREE.Mesh(geometry, materials);
  geosphere.name = `${NAME_PREFIX}${zoom}`;
  geosphere.userData.radius = RADIUS;
  return geosphere;
};

const AVAILABLE_MAP_ZOOMS = [0, 1, 2, 3];
export const getGlobes = (style) => AVAILABLE_MAP_ZOOMS.map((zoom) => (
  getGeosphere({ zoom, style })
));

export const findGlobe = ({ children = [] }) => children
  .find(({ name }) => name.includes(NAME_PREFIX));
