import { CUSTOM_EVENTS } from '../resources/constants';
import { onMouseUp, onMove, onWheel } from './renderer';
import { onChangeView } from './changeView';

export const registerEvents = (target, rendership) => {
  const mouseup = (event) => onMouseUp({ ...rendership, event });
  const mousemove = (event) => onMove({ ...rendership, event });
  const wheel = (event) => onWheel({ ...rendership, event });
  const changeView = ({ detail }) => onChangeView({ ...rendership, detail });

  target.addEventListener('mouseup', mouseup, { capture: false });
  target.addEventListener('mousemove', mousemove, { capture: false });
  target.addEventListener('wheel', wheel, { capture: false });
  document.addEventListener(CUSTOM_EVENTS.changeView, changeView);
};

export const REPLACE_ME = 'prefer default export';
