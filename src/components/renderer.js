import * as THREE from 'three';
import { CUSTOM_EVENTS } from '../resources/constants';
import { getPin, findPin } from './pin';
import { findGlobe } from './geosphere';
import { coordsToUv, uvToCartesian } from '../utils';

const { PI } = Math;
const globeAxis = new THREE.Vector3(0, 1, 0);
const latAxis = new THREE.Vector3(1, 0, 0);

export const resizeRenderer = ({ renderer, camera }) => {
  const { domElement: { width, height, clientWidth, clientHeight } } = renderer;
  const needResize = width !== clientWidth || height !== clientHeight;
  if (needResize) {
    renderer.setSize(clientWidth, clientHeight, false);
    camera.aspect = clientWidth / clientHeight;
    camera.updateProjectionMatrix();
  }
};

export const getIntersect = ({ event = {}, renderer, scene, camera }) => {
  const { domElement: { clientWidth, clientHeight } } = renderer;
  const { clientX = clientWidth / 2, clientY = clientHeight / 2 } = event;
  const mouse = new THREE.Vector2();
  const raycaster = new THREE.Raycaster();
  mouse.x = (clientX / clientWidth) * 2 - 1;
  mouse.y = -(clientY / clientHeight) * 2 + 1;
  raycaster.setFromCamera(mouse, camera);
  const [intersect] = raycaster.intersectObjects(scene.children);
  return intersect;
};

const ANGLE = 8e-4; // ~10x faster than in reality
export const getRender = ({ renderer, scene, camera }) => {
  const { userData } = scene;
  const render = () => {
    if (userData.isPaused) return;
    resizeRenderer({ renderer, camera });
    if (!scene.userData.isTouched) {
      scene.rotateOnAxis(globeAxis, ANGLE);
    }
    renderer.render(scene, camera);
    requestAnimationFrame(render);
  };
  userData.render = render;
  return render;
};

const rotateGlobe = ({ event, scene }) => {
  const SPEED = 4e-3;
  const movementX = event.movementX || event.mozMovementX || event.webkitMovementX || 0;
  const movementY = event.movementY || event.mozMovementY || event.webkitMovementY || 0;
  scene.rotateOnAxis(globeAxis, movementX * SPEED);
  scene.rotateOnWorldAxis(latAxis, movementY * SPEED);
};

let isDragging = false;
export const onMove = (params) => {
  const { event: { buttons }, scene: { userData } } = params;
  isDragging = buttons === 1;
  let cursor = !getIntersect(params) ? 'default' : 'grab';
  if (isDragging) {
    rotateGlobe(params);
    cursor = 'grabbing';
  }
  document.body.style.cursor = cursor;
  if (!userData.isTouched && isDragging) {
    userData.isTouched = true;
  }
};

const removePin = (scene) => {
  const pin = findPin(scene);
  if (pin) {
    scene.remove(pin);
  }
};

const createPin = (params) => {
  const { scene } = params;
  const { userData } = scene;
  const { userData: { radius } = {} } = findGlobe(scene) || {};
  const intersect = getIntersect(params);
  if (!intersect) return;
  const { x, y, z } = uvToCartesian(intersect.uv, radius);
  const pin = getPin();
  pin.position.set(-x, z, y);
  removePin(scene);
  scene.add(pin);
  if (!userData.isTouched) userData.isTouched = true;
};

export const onMouseUp = (params) => {
  if (isDragging) {
    isDragging = false;
    return;
  }
  createPin(params);
};

const updateMapZoom = ({ scene, cameraZoom }) => {
  const { userData } = scene;
  // eslint-disable-next-line no-nested-ternary
  const mapZoom = cameraZoom < 1.4 ? (cameraZoom < 1.2 ? 1 : 2) : 3;
  if (userData.mapZoom !== mapZoom) {
    const [currentObj] = scene.children;
    userData.mapZoom = mapZoom;
    scene.remove(currentObj);
    scene.add(userData.globes[mapZoom]);
  }
  if (cameraZoom > 2.6) {
    document.dispatchEvent(new CustomEvent(CUSTOM_EVENTS.changeView, { detail: 'flat' }));
  }
};

const MIN_ZOOM = 1;
const MAX_ZOOM = 3;
let cameraZoom = MIN_ZOOM;
export const onWheel = ({ event, scene, camera }) => {
  cameraZoom += event.deltaY * -0.1;
  cameraZoom = Math.min(Math.max(cameraZoom, MIN_ZOOM), MAX_ZOOM);
  camera.zoom = cameraZoom;
  camera.updateProjectionMatrix();
  updateMapZoom({ scene, cameraZoom });
};

const resetRotation = (scene) => {
  scene.rotation.set(0, PI / 2, 0);
};

export const jumpGlobeToCoords = (lonLatLike, scene) => {
  resetRotation(scene);
  const { x, y } = coordsToUv(lonLatLike);
  const theta = PI * (1 - y);
  const phi = 2 * PI * x;
  scene.rotateOnAxis(globeAxis, -phi);
  scene.rotateOnWorldAxis(latAxis, PI / 2 - theta);
};
