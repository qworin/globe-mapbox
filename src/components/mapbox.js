import mapboxgl from 'mapbox-gl';

mapboxgl.accessToken = 'pk.eyJ1Ijoicm9kaW9uLXN0cml6aGFrb3YtZGVzIiwiYSI6ImNrM2ZnZDJyZDA0OXozb3J6N3k1Mm9zZ3MifQ.T8MANPypsTlrrEtfaos15A';

export const MAPBOX_ZOOM_THRESHOLD = 4;

export const getMapbox = (center = [0, 0]) => new mapboxgl.Map({
  container: 'mapbox-map',
  style: 'mapbox://styles/mapbox/streets-v11',
  center,
  zoom: MAPBOX_ZOOM_THRESHOLD,
});
