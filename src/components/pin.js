import * as THREE from 'three';

const NAME = 'geopin';

export const getPin = () => {
  const geometry = new THREE.SphereGeometry(0.01, 32, 32);
  const material = new THREE.MeshBasicMaterial({ color: 0xac1344 }); // smth redish
  const pin = new THREE.Mesh(geometry, material);
  pin.name = NAME;
  return pin;
};

export const findPin = ({ children = [] }) => children.find(({ name }) => name === NAME);
