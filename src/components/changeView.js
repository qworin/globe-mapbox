import { getMapbox, MAPBOX_ZOOM_THRESHOLD } from './mapbox';
import { CUSTOM_EVENTS } from '../resources/constants';
import { uvToCoords } from '../utils';
import { getIntersect, jumpGlobeToCoords } from './renderer';

const FLAT_VIEW = 'flat';
const GLOBE_VIEW = 'globe';
const HIDDEN = 'hidden';
const DURATION = 300; // ms
const animation = `zoomIn ease-in ${DURATION}ms`;
const globeWrapper = document.querySelector('#globe');
const flatWrapper = document.querySelector('#mapbox-map');
const map = getMapbox();
let inTransit = false;
console.log('_M_A_P_', map); // TOREMOVE

const onZoomMap = () => {
  if (map.getZoom() < MAPBOX_ZOOM_THRESHOLD) {
    document.dispatchEvent(new CustomEvent(CUSTOM_EVENTS.changeView, { detail: GLOBE_VIEW }));
    map.off('zoom', onZoomMap);
  }
};

const restoreMap = (center = [0, 0]) => map
  .resize()
  .setZoom(MAPBOX_ZOOM_THRESHOLD)
  .on('zoom', onZoomMap)
  .setCenter(center);

const toFlat = ({ scene, camera, renderer }) => {
  const { userData } = scene;
  inTransit = true;
  document.body.style.cursor = '';
  globeWrapper.style.animation = animation;
  flatWrapper.classList.remove(HIDDEN);
  const intersect = getIntersect({ scene, camera, renderer });
  restoreMap(intersect && uvToCoords(intersect.uv));
  setTimeout(() => {
    globeWrapper.style.animation = '';
    globeWrapper.classList.add(HIDDEN);
    inTransit = false;
    userData.isPaused = true;
  }, 0.9 * DURATION);
  if (!userData.isTouched) {
    userData.isTouched = true;
  }
};

const toGlobe = ({ scene }) => {
  const { userData } = scene;
  inTransit = true;
  globeWrapper.classList.remove(HIDDEN);
  globeWrapper.style.animation = `${animation} reverse`;
  userData.isPaused = false;
  userData.render();
  jumpGlobeToCoords(map.getCenter(), scene);
  setTimeout(() => {
    globeWrapper.style.animation = '';
    flatWrapper.classList.add(HIDDEN);
    inTransit = false;
  }, 0.9 * DURATION);
};

export const onChangeView = (params) => {
  const { detail } = params;
  if (detail === FLAT_VIEW && !inTransit) {
    toFlat(params);
  } else if (detail === GLOBE_VIEW && !inTransit) {
    toGlobe(params);
  }
  console.log('@changeView', params); // TOREMOVE
};

export const REPLACE_ME = 'prefer default export';
