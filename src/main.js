import 'mapbox-gl/dist/mapbox-gl.css';
import 'mapbox-gl/dist/style-spec';
import './index.less';

document.addEventListener('DOMContentLoaded', () => {
  console.log('Hello DOM');
  require('./globe-mapbox');
});
