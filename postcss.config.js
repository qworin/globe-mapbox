module.exports = {
    plugins: {
        'postcss-cssnext': {
            browsers: ['last 5 versions', 'safari 5', 'ie 11', 'opera 12.1', 'ios 6', 'android 4']
        },

        'postcss-font-magician': {}
    }
}
