/**
 * USE:
 * – choose zoom (0..19) // NB: zoom > 5 may take a while
 * – execute $ node savetiles.js zoom
 * – execute $ ./combine.sh zoom
 */
const fs = require('fs');
const request = require('request');

const validateStyle = (param) => {
  const styles = {
    dark: 'dark',
    light: 'light',
    outdoors: 'outdoors',
    satellite: 'satellite',
    streets: 'streets',
    default: 'streets',
  };
  const valids = Array.from(new Set(Object.values(styles)));
  if (!!param && !valids.includes(param)) {
    console.error(`Style expected to be one of: ${valids}; received ${param}`);
    console.error(`Set style as ${styles.default}`);
    return styles.default;
  }
  return param || styles.default;
};

const url = ({ x, y, zoom, style }) => {
  const base = `https://api.mapbox.com/v4/mapbox.${style}`;
  const token = 'pk.eyJ1Ijoicm9kaW9uLXN0cml6aGFrb3YtZGVzIiwiYSI6ImNrM2ZnZDJyZDA0OXozb3J6N3k1Mm9zZ3MifQ.T8MANPypsTlrrEtfaos15A';
  return `${base}/${zoom}/${x}/${y}@2x.png?access_token=${token}`;
};
const getTile = ({ index, size }) => ({
  x: index % size,
  y: Math.floor(index / size) % size,
});
const main = (zoom, style) => {
  const size = 2 ** zoom;
  [...Array(size * size)].forEach((_, index) => {
    const { x, y } = getTile({ index, size });
    const coord = zoom ? `.${x}.${y}` : '';
    const filename = `@${zoom}${coord}.png`;
    request(url({ x, y, zoom, style })).pipe(fs.createWriteStream(filename));
  });
  console.log(`${style}@${zoom}`);
};

// Execution
const [,, zoom = 0, style = ''] = process.argv;
main(+zoom, validateStyle(style));
