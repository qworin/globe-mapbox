#!/bin/bash
ZOOM=$1
SIZE=$(node --print "2 ** ${ZOOM}")
HOR=""
for ((X=0; X<$SIZE; X++)) do
  VER=""
  for ((Y=0; Y<$SIZE; Y++)) do
    VER="${VER} @${ZOOM}.${X}.${Y}.png"
  done
  OUT_VER="@${ZOOM}.${X}.png"
  convert -append $VER $OUT_VER
  rm $VER
  HOR="${HOR} ${OUT_VER}"
done
OUT_HOR="@${ZOOM}.png"
convert +append $HOR $OUT_HOR
rm $HOR