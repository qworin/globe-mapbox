const reactRules = require('eslint-config-airbnb/rules/react');
const reacta11yRules = require('eslint-config-airbnb/rules/react-a11y');
const vueRules = require('eslint-plugin-vue/lib');
const ourRules = {
  'no-console': 'off',
  'no-shadow': 'off',
  'object-curly-newline': ['off', { 'multiline': true }],
  'no-plusplus': ['error', { 'allowForLoopAfterthoughts': true }],
  'arrow-parens': ['error', 'always', {
    requireForBlockBody: true,
  }],
  "global-require": "off",
  "operator-linebreak": ["error", "after"],
  'react/jsx-wrap-multilines': ['error', {
    declaration: true,
    assignment: true,
    return: true,
    arrow: true,
  }],
  "react/jsx-curly-brace-presence": ['off', { props: "always", children: "always" }],
  'react/forbid-prop-types': 'off',
  'react/jsx-tag-spacing': ['error', {
    closingSlash: 'never',
    beforeSelfClosing: 'always',
    afterOpening: 'never'
  }],
  "jsx-a11y/click-events-have-key-events": "off",
  "jsx-a11y/no-static-element-interactions": "off",
  "jsx-a11y/anchor-has-content": "off",
}
module.exports = {
  "parser": "babel-eslint",
  "extends": ["eslint:recommended", "airbnb-base"],
  "plugins": [
    "react",
    "jsx-a11y",
    "import",
    "babel"
  ],
  "overrides": [
    { 
      files: ['*.js'], 
      rules: ourRules,
    },
    { 
      files: ['*.jsx'], 
      rules: Object.assign(reactRules.rules, reacta11yRules.rules, ourRules),
    }, { 
      files: ['*.vue'], 
      rules: Object.assign(vueRules.rules, ourRules)
    },
  ],
  "globals": {
    "__DEV__": true,
    "window": true,
    "console": true,
    "module": true,
    "GLOBAL": true,
    "Promise": true
  },
  "env": {
    "browser": true,
    "node": true
  },
  "settings": {
    "import/resolver": {
      "node": {
        "extensions": [
          ".js",
          ".jsx"
        ]
      }
    }
  },
}